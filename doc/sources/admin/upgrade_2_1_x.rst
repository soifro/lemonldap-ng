Upgrade from 2.1.x to 2.1.y
===========================

Update from one minor version to another does not require any particular
action. Please apply general caution as you would with any software:
have backups and a rollback plan ready!

Do not forget to read the release notes of the version you are about to
install for any specific instructions.

