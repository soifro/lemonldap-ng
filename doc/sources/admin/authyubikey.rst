Yubikey
=======


.. attention::

    This module has been replaced by
    :doc:`Yubikey Second Factor<yubikey2f>`\
